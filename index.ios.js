import React, {
  AppRegistry,
}
from 'react-native';

import App from './src/home';

AppRegistry.registerComponent('rnSignature', () => App);