import React, {
  AppRegistry,
}
from 'react-native';

import App from './src/signature';

AppRegistry.registerComponent('rnSignature', () => App);