### 调试

修改接口地址
在/src/signature.js中修改hostname的值即可。

运行命令
```
npm install

react-native start
```
编译

ios
```
react-native run-ios
```
android
```
react-native run-android
```

### 打包
ios
```
http://reactnative.cn/docs/0.23/running-on-device-ios.html#content
```

android
```
http://reactnative.cn/docs/0.23/signed-apk-android.html#content
```