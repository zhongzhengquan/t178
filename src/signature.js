import React, {
	Component, StyleSheet, Text, View, ScrollView, TextInput, ListView, TouchableHighlight,
}
from 'react-native';
import Button from 'apsl-react-native-button';
import _ from "underscore";
import GiftedListView from "react-native-gifted-listview";
import SignatureCapture from "react-native-signature-capture";

const hostname = "http://192.168.16.117:8360";

export default class Signature extends Component {
	constructor(props) {
		super(props);
		var ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2
		});
		this.state = {
			queryWords: '1期朱莎井控班',
			currentStudent: '',
			dataSource: ds.cloneWithRows([]),
			datas: new Array(),
			loading: false,
			who: '',
		};
	}
	componentDidMount() {
		this.setState({
			who: '请在下方区域签名',
		});
	}
	_search() {
		let queryWords = this.state.queryWords;
		console.log(queryWords);
		if (queryWords) {
			try {
				this.setState({
					loading: true
				});
				let url = hostname + "/students?page_size=100&page=1&queryWords=" + queryWords;
				fetch(url)
					.then(res => res.json())
					.then(res => {
						this.setState({
							dataSource: this.state.dataSource.cloneWithRows(res),
							datas: res,
							loading: false,
						});
					});
			} catch (error) {
				console.error(error);
				this.setState({
					loading: false,
				});
			}
		}
	}
	_signature(rowData) {
		console.log(JSON.stringify(rowData));
		this.setState({
			currentStudent: rowData._id,
			who: rowData.student_name + ',请在下方区域签名',
		});
	}
	_renderRowView(rowData: object, sectionID: number, rowID: number, highlightRow) {
		return (
			<View style={styles.table}>
					<View style={styles.body}><Text style={styles.title}>{rowData.student_name}</Text></View>
					<View style={styles.body}><Text style={styles.title}>{rowData.id_number}</Text></View>
					<View style={styles.body}><Text style={styles.title}>{rowData.phone_number}</Text></View>
					<View style={styles.body}><Text style={styles.title}>{rowData.erp_number}</Text></View>
					<View style={styles.body}><Text style={styles.title}>{rowData.class_name}</Text></View>
					<View style={styles.body}>
						<Button						
						  style={styles.signatureBtn}
						  textStyle={{fontSize: 12}}
						  onPress={() => {
						  	highlightRow(true);
						  	this._signature(rowData)
						  }}
						  >
						  签到
						</Button>
					</View>
		      	</View>
		);
	}
	_onSaveEvent(result) {
		console.log(JSON.stringify(result.encoded));
		console.log(this.state.currentStudent);
		if (!_.isEmpty(result) && this.state.currentStudent != '') {
			let base64 = "data:image/png;base64," + result.encoded.replace(/\n/g, "");
			let url = hostname + "/students/";
			fetch(url, {
					method: 'PUT',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						id: this.state.currentStudent,
						signature: base64,
					})
				})
				.then(res => res.json())
				.then(res => {
					let newdatas = _.reject(this.state.datas, (data) => {
						return data._id == this.state.currentStudent
					});
					this.setState({
						dataSource: this.state.dataSource.cloneWithRows(newdatas),
						who: '请在下方区域签名',
						datas: newdatas,
						currentStudent: '',
					});
					alert("签名成功");
				});
		} else if (_.isEmpty(result)) {
			alert("请在下方区域签名!!!");
		} else {
			alert("请选择学生！");
		}
	}
	_onResetEvent(){
		console.log("_onResetEvent");
	}
	render() {
		return (
			<View style={{flex:2,flexDirection: 'row',}}>
				<View style={{flex:1,}}>
					<View style={{height:50,flexDirection: 'row'}}>
						<View style={{flex:1,flexDirection: 'row'}}>
							<View style={{flex:6,flexDirection: 'row'}}>
								<View style={{flex:5}}>
									<TextInput 
										onChangeText={(text) => this.setState({queryWords:text})}
										value={this.state.queryWords}
										/>
						      	</View>
								<View style={{flex:1,backgroundColor: '#FFFFFF',}}>
									<Button
									  isLoading={this.state.loading}
									  style={styles.searchBtn}
									  textStyle={{fontSize: 18}}
									  onPress={this._search.bind(this)}
									  >
									  查询
									</Button>
					      		</View>
					      	</View>
						</View>
			      	</View>
			      	<View style={{flexDirection: 'row'}}>
						<View style={styles.table}>
							<View style={styles.header}><Text style={styles.title}>姓名</Text></View>
							<View style={styles.header}><Text style={styles.title}>身份证号</Text></View>
							<View style={styles.header}><Text style={styles.title}>手机号</Text></View>
							<View style={styles.header}><Text style={styles.title}>ERP编号</Text></View>
							<View style={styles.header}><Text style={styles.title}>班级名称</Text></View>
							<View style={styles.header}><Text style={styles.title}>操作</Text></View>
			      		</View>
			      	</View>
					    <ListView
					      dataSource={this.state.dataSource}
					      renderRow={this._renderRowView.bind(this)}
					    />
		        </View>
				<View style={{flex:1,backgroundColor:'#f2f2f2',}}>
					<View style={{height:50,alignSelf: 'center',justifyContent: 'center',}}>
						<Text>
							{this.state.who}
						</Text>
					</View>
					<View style={{flex:1}}>
						<SignatureCapture
				          rotateClockwise={true}
				          square={true}
				          onResetEvent={this._onResetEvent.bind(this)}
				          onSaveEvent={this._onSaveEvent.bind(this)}/>
					</View>
		        </View>
      		</View>

		);
	}
};

const styles = StyleSheet.create({
	table: {
		backgroundColor: '#FFFFFF',
		flex: 6,
		flexDirection: 'row',
	},
	header: {
		backgroundColor: '#F2F2F2',
		flex: 1,
	},
	body: {
		alignSelf: 'center',
		backgroundColor: '#FFFFFF',
		flex: 1,

	},
	title: {
		textAlign: 'center',
		justifyContent: 'center',
		alignItems: 'center',
	},
	sigature: {
		backgroundColor: '#EFEFF4',
		flex: 6,
	},
	searchBtn: {
		justifyContent: 'center',
		borderColor: '#27ae60',
		backgroundColor: '#2ecc71'
	},
	signatureBtn: {
		height: 20,
		borderColor: '#27ae60',
		backgroundColor: '#2ecc71'
	},
});