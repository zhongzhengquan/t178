import React, {Component,StyleSheet,Text,View} from 'react-native';
import {Scene, Reducer, Router, Switch, TabBar, Modal, Schema, Actions} from 'react-native-router-flux'
import SignatureCapture from 'react-native-signature-capture';
import Button from 'apsl-react-native-button';
import Dimensions from 'Dimensions';

import Signature from './signature';
import Statistics from './statistics';
import Home from './home';

var {height, width} = Dimensions.get('window');

const reducerCreate = params=>{
    const defaultReducer = Reducer(params);
    return (state, action)=>{
        console.log("ACTION:", action);
        return defaultReducer(state, action);
    }
};

export default class App extends Component {
  render() {
    return (
        <Router createReducer={reducerCreate} sceneStyle={{backgroundColor:'#F7F7F7'}}>
            <Scene key="modal" component={Modal} >
              <Scene key="root">
                <Scene key="home" component={Home}/>
                <Scene key="signature" component={Signature}/>
                <Scene key="statistics" component={Statistics}/>
              </Scene>
            </Scene>
        </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
    // flexDirection:'row',
  },
  left: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  right: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'green',
  },
});