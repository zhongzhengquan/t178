import React, {Component,StyleSheet,Text,View} from 'react-native';
import {Actions, Scene, Router} from 'react-native-router-flux';
import SignatureCapture from 'react-native-signature-capture';
import Button from 'apsl-react-native-button';

export default class App extends Component {
  _onPress() {
    console.log(1);
  }
  render() {
    return (
      <View style={styles.container}>
          <View style={styles.left}>
            <Button style={{backgroundColor: '#ffffff'}} textStyle={{fontSize: 18}} onPress={this._onPress}>
              签到入口
            </Button>
          </View>
          <View style={styles.right}>
            <Button style={{backgroundColor: '#ffffff'}} textStyle={{fontSize: 18}} onPress={this._onPress}>
              统计入口
            </Button>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
    // flexDirection:'row',
  },
  left: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  right: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'green',
  },
});